<?php

#superglobal variable example

// $y = 30;
// function sum()
// {
//     return $GLOBALS['y'];
// }

// echo sum();

#another way same code
// $y = 30;
// $z = 24;
// function sum()
// {
//     global $y,$z;
//     return $y+$z;
// }

// echo sum();


#important exmple
// $a=5;
// function sum()
// {
//     $a=10;
//     return $a;
// }
// echo sum();


#annonymuse function used by global escope
// $y=20;
// $sum = function () use ($y)
// {
//     return $y;
// };
// echo $sum();

#annonymuse function used by global escope
// $y=20;
// $sum = function ($num1) use ($y)
// {
//     return $num1+$y;
// };
// echo $sum(5);
















































