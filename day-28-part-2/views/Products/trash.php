<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Products List</title>
</head>
<body>
    
    <?php
        include_once '../../vendor/autoload.php';
        use Sujan\Product;

        $productObject = new Product();
        $products = $productObject -> trash();

        // echo '<pre>';
        // print_r($products);
    ?>

    <a href="index.php">List</a>

    <?php
    if(isset($_SESSION['message'])){
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
    ?>


    <table border="1">
        <thead>
            <tr>
                <th>SL</th>
                <th>Title</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>

            <?php

            $sl=1;

            foreach ($products as $product){ ?>
            <tr>
                <td> <?= $sl++ ?> </td>
                <td> <?= $product['title'] ?> </td>
                <td>
                    
                   <!-- <a href="show.php?id=<?= $product['id'] ?>"> Show </a> -->
                   <a href="restore.php?id=<?= $product['id'] ?>"> Restore </a>
                   <a href="parment_delete.php?id=<?= $product['id'] ?>" onclick="return confirm('Are you sure want to delete permanetly ?')" >Parmanent Delete </a>
                
                </td>
            </tr>
            <?php } ?>

        </tbody>
    </table>

</body>
</html>