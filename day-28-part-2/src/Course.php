<?php 
namespace Sujan;

use PDO;
use PDOException;

class Course
{

    public $conn;

    public function __construct()
    {
        try{
            // session_start();

            $this -> conn = new PDO("mysql:host=localhost;dbname=crud", "root", "1234");
            $this -> conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch(PDOException $exception){

         $_SESSION['error'] = $exception->getMessage();
            header('location:../views/Products/create.php');   
        }
}


public function courseByProductId($id = null)
{

if($id){
    $query = "select * from courses where Product_id=$id  ";
}else{
    $query = "select * from courses ";
}

    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute();

    $data = $stmt->fetchAll();

    return $data;
}

public function show($id)
{

    $query = "select * from courses where id=:product_id";
    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute([
        'product_id' => $id
    ]);

    $data = $stmt->fetch();

    return $data;
}



}