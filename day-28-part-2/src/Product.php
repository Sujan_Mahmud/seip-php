<?php 
namespace Sujan;

use PDO;
use PDOException;

class Product{

    public $conn;

    public function __construct()
    {
        try{
            session_start();

            $this -> conn = new PDO("mysql:host=localhost;dbname=crud", "root", "1234");
            $this -> conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch(PDOException $exception){

         $_SESSION['error'] = $exception->getMessage();
            header('location:../views/Products/create.php');   
        }
}



public function index()
{
    $query = "select * from products where is_deleted is null ";
    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute();

    $data = $stmt->fetchAll();

    return $data;
}


public function store($data)
{
    try{
        
        //for file uploade statement,validation start.

        $fileName = $_FILES['picture']['name'];
        $tempName = $_FILES['picture']['tmp_name'];
        $explodedArray = explode('.', $fileName);
        $fileExtension = end($explodedArray);

        $acceptableImageType = ['png', 'jpg', 'gif', 'jpeg', 'heic'];
        if(!in_array($fileExtension, $acceptableImageType)){
            $_SESSION['error'] = 'Invalid image type. Only '.implode(', ', $acceptableImageType ). ' Allowed';
            header('location:../Products/create.php');  
        }

        $uniqueImageName = time().$fileName;
        move_uploaded_file($tempName, '../../assets/images/'.$uniqueImageName);

        //for file upload statement,validation end.

        $title = $data['title'];
        $description = $data['description'];
        
        $query = "insert into products(title, description, picture) values (:product_title, :product_descritption, :product_picture)";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute([
        
                'product_title' => $title,
                
                'product_descritption' => $description,

                'product_picture' => $uniqueImageName,
                              
                ]);

                $_SESSION['message'] = 'Successfully Created !';
                header('location:../Products/index.php'); 
                
        }catch (PDOException $exception){

            $_SESSION['error'] = $exception->getMessage();
            header('location:../Products/create.php');  
        }

    }

public function show($id)
{

    $query = "select * from products where id=:product_id";
    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute([
        'product_id' => $id
    ]);

    $data = $stmt->fetch();

    return $data;
}

public function edit($id)
{

    $query = "select * from products where id=:product_id";
    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute([
        'product_id' => $id
    ]);

    $data = $stmt->fetch();

    return $data;
}

public function update($data)
{
    try{


        $id = $data['id'];
        $title = $data['title'];
        $description = $data['description'];
        
        $query = "update products set title=:product_title, description=:product_description where id=:product_id";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute([
        
                'product_title' => $title,
                
                'product_description' => $description,

                'product_id' => $id,
                              
                ]);

                $_SESSION['message'] = 'Successfully Updated !';
                header('location:../Products/index.php'); 
                
        }catch (PDOException $exception){

            $_SESSION['error'] = $exception->getMessage();
            header('location:../Products/edit.php');  
        }

    }

public function destroy($id)
{
    try{
        // $query = "delete from products where id=:product_id";
        $query = "update products set is_deleted=:deleted where id=:product_id";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute([
            'deleted' => 1,
            'product_id' => $id
        ]);
        $_SESSION['message'] = 'Successfully Deleted !';
        header('location:../Products/index.php');

    }catch (PDOException $exception){

        $_SESSION['error'] = $exception->getMessage();
        header('location:../Products/index.php');  
    }

}

public function restore($id)
{
    try{
        // $query = "delete from products where id=:product_id";
        $query = "update products set is_deleted=:deleted where id=:product_id";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute([
            'deleted' => null,
            'product_id' => $id
        ]);
        $_SESSION['message'] = 'Successfully Restore !';
        header('location:../Products/trash.php');

    }catch (PDOException $exception){

        $_SESSION['error'] = $exception->getMessage();
        header('location:../Products/trash.php');  
    }

}

public function trash()
{
    $query = "select * from products where is_deleted=1 ";
    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute();

    $data = $stmt->fetchAll();

    return $data;
}

public function delete($id)
{
    try{
        $query = "delete from products where id=:product_id";
        // $query = "update products set is_deleted=:deleted where id=:product_id";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute([
            'product_id' => $id
        ]);
        $_SESSION['message'] = 'Successfully Deleted !';
        header('location:../Products/trash.php');

    }catch (PDOException $exception){

        $_SESSION['error'] = $exception->getMessage();
        header('location:../Products/trash.php');  
    }

}


}