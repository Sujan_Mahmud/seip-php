<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Categories List</title>
</head>
<body>
<?php
        include_once '../../src/Categorie.php';
        $categorieObject = new Categorie();
        $Categories = $categorieObject -> index();

        // echo '<pre>';
        // print_r($products);
    ?>

    <a href="create.php">Add New</a>

    <?php
    if(isset($_SESSION['message'])){
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
    ?>


    <table border="1">
        <thead>
            <tr>
                <th>SL</th>
                <th>Title</th>
                <th>Description</th>
            </tr>
        </thead>

        <tbody>

            <?php

            $sl=1;

            foreach ($Categories as $categorie){ ?>
            <tr>
                <td> <?= $sl++ ?> </td>
                <td> <?= $categorie['title'] ?> </td>
                <td> <?php echo $categorie['description'] ?> </td>
            </tr>
            <?php } ?>

        </tbody>
    </table>
</body>
</html>