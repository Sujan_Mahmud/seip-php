<?php 

class Categorie{
		public $conn;
		public function __construct()
		{
		
		try{
			session_start();
		
		$this -> conn = new PDO("mysql:host=localhost;dbname=crud", "root", "1234");
		$this -> conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		}catch(PDOException $exception){
		
		$_SESSION['error'] = $exception->getMessage();
			header('location:../views/Categories/create.php');  

		}
    }

public function index()
{
	$query = "select * from categories";
	$stmt = $this -> conn -> prepare($query);
	$stmt -> execute();

	$data = $stmt->fetchAll();

	return $data;
}
	
		



public function store($data)
{
	try{
		$title = $data['title'];
		$description = $data['description'];
		
		$query = "insert into categories(title, description) values(:categorie_title, :categorie_descritption)";
		$stmt = $this -> conn -> prepare($query);
		$stmt -> execute([
		
				'categorie_title' => $title,
				
				'categorie_descritption' => $description,
				
				
				]);

				$_SESSION['message'] = 'Successfully Created !';
                header('location:../Categories/index.php'); 
				
		}catch (PDOException $exception){

            $_SESSION['error'] = $exception->getMessage();
            header('location:../Categories/create.php');  
		}
	}


}