<?php 
#array in php
// array is a types of variable which is store on multiple data
// array are basically tree types
// 1.index array
// 2.associative array
// 3.array 

#index array example
// $skill = [
//     'html',
//     'css',
//     'bootstrup',
//     'java script',
//     'php'
// ];

// echo '<pre>';
// print_r($skill);
// echo '</pre>';

// echo $skill[1];

#associative array te key ta bole dite hoi 
#associative array example
// $array =[
//     'skill-1' => 'html',
//     'skill-2' => 'css',
//     'skill-3' => 'bootstrup',
//     'skill-4' => 'java script',
    
// ];

// $array ['skill-4'] = 'laravel';

// echo '<pre>';
// print_r($array);
// echo '</pre>';

// echo $array['skill-3'];

#valu assagin out in main array 
// $arr = [];
// $arr [1] = 'html';
// $arr [2] = 'css';
 
// echo '<pre>';
// print_r($arr);



#nestecd array example
// $array = [
//     $array1 = [
//         'name-1' => 'sujan',
//         'name-2' => 'babul',
//         'name-3' => 'rony'
//     ],
//     $array2 = [
//         'name-1' => 'sujan',
//         'name-2' => 'babul',
//         'name-3' => 'rony'
//     ],
// ];

// echo '<pre>';
// print_r($array);
// echo '</pre>';



#just example
// $array = [5,6];
// $array ['status']= true;
// $array []= 1;
// $array ['type'] = 'hello';
// $array [] = 10;

// echo '<pre>';
// print_r($array);
// echo '</pre>';

// var_dump($array);


#multi-dimensional array example
// $person = [
//     'name' => 'sujan mahmud',
//     'skill' => [
//         'html',
//         'css',
//         'php'
//     ]
// ];

// echo $person['name'];

// echo '<pre>';
// print_r($person['skill']);
// echo '</pre>';

#another complex example MDA
#versin ta k print korte hove incomplete
// $person = [
//     'name' => 'sujan mahmud',
//     'skill' => [
//         'html'=>[
//             'version'=>[4,5]
//         ],
//         'css',
//         'php'
//     ]
// ];


// echo '<pre>';
// print_r($person['version']);
// echo '</pre>';

