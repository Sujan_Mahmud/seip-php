<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Products Create</title>
</head>
<body>


    <?php
        include_once '../../src/Product.php';

        $productObject = new Product();
        $product = $productObject ->edit($_GET['id']);

        // print_r($product);
    ?>

    <?php
    if(isset($_SESSION['error'])){
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }
    ?>

<a href="index.php">List</a> 

    <form action="update.php"  method="post">
        
        <input type="hidden" name="id" value="<?=$product['id']?>" /> <br>
        <input type="text" name="title" value="<?=$product['title']?>" placeholder="Enter your title here" /><br>

        <label for="descripion"> Description: </label><br>

        <textarea name="description" rows="5" id="description">

            <?=$product['description']?>

        </textarea><br>

        <button type="submit">Update</button>

    </form>

</body>
</html>