<?php
include_once '../../src/Product.php';

$productObject = new Product();
$product = $productObject -> show($_GET['id']);

?>

<a href="index.php">Product List</a>

<h1><?= $product['title'] ?></h1>
<p><?= $product['description'] ?></p>
<img src="../../assets/images/<?= $product['picture'] ?>" />