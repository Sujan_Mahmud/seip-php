<?php
//how to use this key word.
//how to use magic construct in a class.
//this keyword work like as class-obj .
//magic construct work same time when call a object by default.


// class Calculator2
// {
//     public $number1;
//     public $number2;

//     //magic construct use
//     public function __construct($num1, $num2)
//     {
//         $this->number1=$num1;
//         $this->number2=$num2;
//     }

//     //method
//     public function sum()
//     {
//         return $this->number1 + $this->number2;
//     }

//     public function result()
//     {
//         return 'Result';
//     }

// }

// $calculatorObj = new Calculator2(10, 20);

// echo 'Your sum : ' .$calculatorObj -> sum();