<?php

#const = use korte hole escope regulation sign (::) dite hobe.
#constant er variable name always capital dite hobe it's convention.
#constant k class er vitor theke use kortte chile (self::) use korte hobe.

#constant example using escope regulation :
// class ParentClass
// {
//     const PI = 3.1416;

//     public function intro()
//     {
//         echo 'i\'m parent class method';
//     }
// }

// $ParentClassObj = new ParentClass();
// echo $ParentClassObj :: PI;  //use escope regulation sign (::)

#static example :
// class ParentClass
// {
//     static $staticProperty = 'static property of parent class';

//     public function intro()
//     {
//         echo 'i am from parent class method';
//     }
// }

// $ParentClassObj = new ParentClass();
// echo $ParentClassObj :: $staticProperty ; // use static property (::$)

#another example same code
// class ParentClass
// {
//     static $staticProperty = 'static property of parent class';

//     public function intro()
//     {
//         echo 'i am from parent class method . '.self::$staticProperty;
//     }
// }

// $ParentClassObj = new ParentClass();
// echo $ParentClassObj ->intro() ;

#all in one example :
// class ParentClass
// {
//     const PI = 3.1416;
//     static $staticProperty = 'static property of parent class';

//     public $publicProperty = 'i am public property of parent class';
//     protected $protectedProperty = 'i am protected property of parent class';

//     public function intro()
//     {
//         echo 'i am from parent class method';
//     }
// }

// $ParentClassObj = new ParentClass();
// echo $ParentClassObj :: PI ; //accessing const property
// echo $ParentClassObj :: $staticProperty; // accessing static property
// echo $ParentClassObj -> publicProperty ; // accessing public property


#another way all in one example :
// class ParentClass
// {
//     const PI = 3.1416;
//     static $staticProperty = 'static property of parent class';

//     public $publicProperty = 'i am public property of parent class';
//     protected $protectedProperty = 'i am protected property of parent class';

//     public function intro()
//     {
//         echo 'i am from parent class method.<br>' .self::$staticProperty;
//     }
// }

// $ParentClassObj = new ParentClass();
// echo $ParentClassObj -> intro() ; //accessing const property
// echo $ParentClassObj -> intro(); // accessing static property
// echo $ParentClassObj -> publicProperty ; // accessing public property










