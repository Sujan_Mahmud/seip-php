<?php

#for understandin see this example otherwise no need.
// include_once 'src/Laptop/Recorder.php';
// include_once 'src/Mobile/Recorder.php';
// include_once 'src/Laptop/Drivers/Graphics.php';


// $LaptopRecorderObj = new Laptop\Recorder();
// echo '<br>';
// $MobileRecorderObj = new Mobile\Recorder();
// echo '<br>';
// $DriverGraphicsObj = new Laptop\Graphics\Graphics();  //aghe namespace theke level location nea aste hobe.(Laptop\Graphics)


#alternative same code use in righ way is called fully qalified namespace follow it.
// include_once 'src/Laptop/Recorder.php';
// include_once 'src/Mobile/Recorder.php';
// include_once 'src/Laptop/Drivers/Graphics.php';
// include_once 'src/Mobile/Drivers/Graphics.php';


// use Laptop\Recorder as LaptopRecorder;
// use Mobile\Recorder as mobileRecorder;
// use Laptop\Graphics\Graphics as Graphics;


// $LaptopRecorderObj = new LaptopRecorder();
// echo '<br>';
// $MobileRecorderObj = new mobileRecorder();
// echo '<br>';

// $LDriverGraphicsObj = new Graphics();
// echo '<br>';
// $MDriverGraphicsObj = new Mobile\Graphics\Graphics();


#auto loading avoid again and againg file include problem.
