<?php 

class Product{

    public $conn;

    public function __construct()
    {
        try{
            session_start();

            $this -> conn = new PDO("mysql:host=localhost;dbname=crud", "root", "1234");
            $this -> conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch(PDOException $exception){

         $_SESSION['error'] = $exception->getMessage();
            header('location:../views/Products/create.php');   
        }
}



public function index()
{
    $query = "select * from products";
    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute();

    $data = $stmt->fetchAll();

    return $data;
}


public function store($data)
{
    try{

        $title = $data['title'];
        $description = $data['description'];
        
        $query = "insert into products(title, description) values(:product_title, :product_descritption)";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute([
        
                'product_title' => $title,
                
                'product_descritption' => $description,
                              
                ]);

                $_SESSION['message'] = 'Successfully Created !';
                header('location:../Products/index.php'); 
                
        }catch (PDOException $exception){

            $_SESSION['error'] = $exception->getMessage();
            header('location:../Products/create.php');  
        }

    }

public function show($id)
{

    $query = "select * from products where id=:product_id";
    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute([
        'product_id' => $id
    ]);

    $data = $stmt->fetch();

    return $data;
}

public function edit($id)
{

    $query = "select * from products where id=:product_id";
    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute([
        'product_id' => $id
    ]);

    $data = $stmt->fetch();

    return $data;
}

public function update($data)
{
    try{

        $id = $data['id'];
        $title = $data['title'];
        $description = $data['description'];
        
        $query = "update products set title=:product_title, description=:product_description where id=:product_id";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute([
        
                'product_title' => $title,
                
                'product_description' => $description,

                'product_id' => $id,
                              
                ]);

                $_SESSION['message'] = 'Successfully Updated !';
                header('location:../Products/index.php'); 
                
        }catch (PDOException $exception){

            $_SESSION['error'] = $exception->getMessage();
            header('location:../Products/edit.php');  
        }

    }

public function destroy($id)
{
    try{
        $query = "delete from products where id=:product_id";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute([
            'product_id' => $id
        ]);
        $_SESSION['message'] = 'Successfully Deleted !';
        header('location:../Products/index.php');

    }catch (PDOException $exception){

        $_SESSION['error'] = $exception->getMessage();
        header('location:../Products/index.php');  
    }

}



}