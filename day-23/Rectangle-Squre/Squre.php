<?php 

class Squre extends Shape{
    
    public $length;

    public function __construct($l ){
        $this->length = $l;
    }

    public function Area(){
        return $this -> length * $this -> length;
    }

    public function display(){
        echo "Area of Squre is : " .$this ->Area();
    }
}