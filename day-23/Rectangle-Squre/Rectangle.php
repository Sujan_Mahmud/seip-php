<?php 

class Rectangle extends Shape{
    public $length;
    public $width;

    public function __construct($l, $w){
        $this->length = $l;
        $this ->width = $w;
    }

    public function Area(){
        return $this -> length * $this -> width;
    }

    public function display(){
        echo "Area of Rectangle is : " .$this ->Area();
    }

}