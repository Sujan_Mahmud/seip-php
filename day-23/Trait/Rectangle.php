<?php 

include_once("Shape.php");
include_once("Displayer.php");

class Rectangle extends Shape{

    
    use Displayer;   //use in trait tools in main class

    public $length;
    public $width;

    public function __construct($l, $w){
        $this->length = $l;
        $this ->width = $w;
    }

    public function Area(){
        return "Area of Rectangle is : " .$this->length * $this->width;
    }

}