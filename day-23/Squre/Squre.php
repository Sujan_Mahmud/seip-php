<?php 

include_once("Shape.php");
include_once("Displayer.php");

class Squre extends Shape{

    use Displayer;

    public $length;

    public function __construct($l ){
        $this->length = $l;
    }

    public function Area(){
        return "Area of square is : " .$this -> length * $this -> length;
    }

}