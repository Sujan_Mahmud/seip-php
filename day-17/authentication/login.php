<?php
session_start();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>get-post-session</title>
</head>
<body>

    <?php
    if(isset($_SESSION['message'])){
         
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
   ?>

    <form action="login-process.php" method="POST">

        <input name="username" type="text" placeholder="Enter your name">
        <input name="password" type="password" placeholder="Enter your password">
        <button type="submit">Login</button>

    </form>
</body>
</html>