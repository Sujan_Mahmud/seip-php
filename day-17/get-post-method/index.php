<!-- 

$_GET = it's show data in  url . 
$_POST = it's does not show in url . more secure from GET method
$_REQUEST =
$_SESSION =
$_COOKIE =
$_GLOBALS =
$_FILES =
$_ENV =

-->



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>superGlobal variable</title>
</head>
<body>
    <form action="form-data.php" method="GET">
        <h3>form GET method data Passed</h3>
        <input name="name" type="text" placeholder="Enter Your Name ">
        <input name="password" type="password" placeholder="Enter Your Password">
        <button type="submit">Login</button>
    <br>  <!--url e data show kore--> 


    <form action="form-data.php" method="POST">
        <h3>form POST method data Passed</h3>
        <input name="name" type="text" placeholder="Enter Your Name ">
        <input name="password" type="password" placeholder="Enter Your Password">
        <button type="submit">Login</button>
    </form>   <!--url e data show kore na-->

    
</body>
</html>