<?php
#OOP start here concept
// clsaa = blue print/templete/noksha/planning .
// object = instance of a class.
// property= variable but call korar somoi $ use kora jabe na.
// method = like function same to same.
//access modifire = public/private/protect.
//-> = obj access operator.
//new = obj create korar somoi use korte hoi. 
//convenction follow must StudlyCase.

#Example all use here:
class Calculator
{
    //properties
    public  $num1 = 10;
    public $num2 = 20;

    //method
    public function sum($perameter1, $perameter2)
    {
        return $perameter1 + $perameter2 ;
    }

}
$calculatorObj = new Calculator();

// echo $calculatorObj -> num1; //for property use.
echo $calculatorObj -> num2;

// echo $calculatorObj -> sum(100,200); //for method use.

