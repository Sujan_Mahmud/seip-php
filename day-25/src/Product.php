<?php 

class Product{

    public $conn;

    public function __construct()
    {
        try{
            session_start();

            $this -> conn = new PDO("mysql:host=localhost;dbname=crud", "root", "1234");
            $this -> conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch(PDOException $exception){

         $_SESSION['error'] = $exception->getMessage();
            header('location:../views/Products/create.php');   
        }
}



public function index()
{
    $query = "select * from products";
    $stmt = $this -> conn -> prepare($query);
    $stmt -> execute();

    $data = $stmt->fetchAll();

    return $data;
}



public function store($data)
{
    try{

        $title = $data['title'];
        $description = $data['description'];
        
        $query = "insert into products(title, description) values(:product_title, :product_descritption)";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute([
        
                'product_title' => $title,
                
                'product_descritption' => $description,
                              
                ]);

                $_SESSION['message'] = 'Successfully Created !';
                header('location:../Products/index.php'); 
                
        }catch (PDOException $exception){

            $_SESSION['error'] = $exception->getMessage();
            header('location:../Products/create.php');  
        }

    }
}