<h1>hello php function</h1>
<?php
#function syntext:
// function function_name(){

//     echo 'welcome';

// }
// Function_name();

#convention follow camelcase
// function functionName(){
//     return 'welcome';
//     echo 'pondit'; // not excute cz already function called
// }
// echo functionName();

#agrgument pass in return
// function sum()
// {
//     return 10+20;
// }

// echo sum();

#another way argument pass and receved by parameter 
// function sum($num1,$num2) //recevied by parameter
// {
//     return $num1+$num2;
// }

// echo sum(10 , 20); //argument pass


#single argument pass by using parameterr
// function greet($name) //here name parameter
// {
//     echo 'welcome to '.$name;
// }
// greet('pondit'); //argument

#multiple argument pass by using multiple parameter
// function greet($name , $address)
// {

//     echo 'welcome to '.$name.'.address '.$address;
// }
// greet('sujan', 'uttara dhaka');

#anoter example 
// function greet($name,$skill,$mobile)
// {

//     echo 'welcome to '.$name. '.your skill '.$skill. '.you mobile number : '.$mobile;
// }
// greet('sujan',' php ','01710626476');

#ojotha skip it
// function greet($name , $address)
// {

//     echo 'welcome to '.$name.'.address '.$address;
// }

// function sayHello($name , $address)
// {

//     echo 'welcome to '.$name.'.address '.$address;
// }

// sayHello('babul', 'uttara dhaka');
// greet('sujan', 'uttara dhaka');

#default parameter 
// function sayHello($name='sujan')
// {

//    return 'Hello Programmer '.$name;

// }
// echo sayHello('Developers');

#Type-custing  use example
// $num1=5;
// $num2=(int)5.5;

// echo 'your summetion : ' .$num1+$num2;


#Type-hinting example
// function sum($num1, float $num2)
// {

//     return $num1+$num2;

// }
// echo sum(10,5.5);

#another exaample say error
// function sum($num1, int $num2)
// {

//     return $num1+$num2;

// }
// echo sum(10 , 'string');



#Home work 
// annonymous function 
// closure 
// arrow function 
// #array home work

// $student = [

    

//     'cse'=>[
//         'name'=>'Sujan','email'=>'sujan@gmail.com',
//         'name'=>'Rubel','email'=>'rubel@gmail.com',
//     ];
//     'bba'=>[
//         'name'=>'Sagor','email'=>'sagor@gmail.com',
//         'name'=>'Tamim','email'=>'tamim@gmail.com',
//     ];
// ];


// output:

// Department: CSE
// Student:
// 1.Name: Sujan, Email:..........
// 2.Name: Rubel, Email....... 
// Department: BBA
// Student:
// 1.Name: Sagor, Email.......... 
// 2.Name: Tamim, Email......... 












