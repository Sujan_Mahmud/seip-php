<?php

$students = [
    'cse' => [
        ['name' => 'Himel', 'email' => 'himel@email.com', 'result' => '4'],['name' => 'Zarin', 'email' => 'zarin@email.com', 'result' => '4'],
    ],
    'bba' => [
        ['name' => 'Tamim', 'email' => 'tamim@email.com', 'result' => '4'],['name' => 'Kafi', 'email' => 'kafi@email.com', 'result' => '3.9'],
    ],
];

foreach ($students as $key => $department) {
    echo 'Department: '.strtoupper($key).'<br>Students: <br>';
    $i = 1;

    foreach ($department as $student) {
        echo $i.'. Name: '.$student['name'].', Email: '.$student['email'].', Result:'.$student['result'].'<br>';
        ++$i;
    }
    echo '<br>';
}


# output -
//--------------
// Department: CSE
// Students:
// 1. Name: Himel, Email: himel@email.com, Result:4
// 2. Name: Zarin, Email: zarin@email.com, Result:4

// Department: BBA
// Students:
// 1. Name: Tamim, Email: tamim@email.com, Result:4
// 2. Name: Kafi, Email: kafi@email.com, Result:3.9