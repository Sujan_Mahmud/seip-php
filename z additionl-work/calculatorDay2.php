<?php

class calculatorDay2
{
    public $number1;
    public $number2;

    public function __construct($num1, $num2)
    {
        $this->number1 = $num1;
        $this->number2 = $num2;
    }

    public function sum()
    {
        return $this->number1 + $this->number2;
    }

     public function sub()
    {
        return $this->number1 - $this->number2;
    }

     public function div()
    {
        return $this->number1 / $this->number2;
    }

     public function mul()
    {
        return $this->number1 * $this->number2;
    }
}

$calculatorObj = new calculatorDay2(10, 20);
echo "Summation : " .$calculatorObj->sum() ."<br>";
echo  "Subtraction : " .$calculatorObj->sub() ."<br>";
echo  "Division : " . $calculatorObj->div() ."<br>";
echo  "Multiplication : " . $calculatorObj->mul() ."<br>";
